var txt = "quilt";   
$(document).ready(function(){
    checkLike();
    $("#txt1").on("click", function() {
        var txt = $(this).text();
        $.ajax({
            url: "data/" + txt,
            datatype: 'json',
            success: function(data){
                var obj = jQuery.parseJSON(data)
                renderHTML(obj);
            },
            error: function(error){
                alert("Books not found");
            },
        })
    });
    $("#txt2").on("click", function(){
        var txt = $(this).text();
        $.ajax({
            url: "data/" + txt,
            datatype: 'json',
            success: function(data){
                var obj = jQuery.parseJSON(data)
                renderHTML(obj);
            },
            error: function(error){
                alert("Books not found");
            },
        })
    });
    $("#txt3").on("click", function(){
        var txt = $(this).text();
        $.ajax({
            url: "data/" + txt,
            datatype: 'json',
            success: function(data){
                var obj = jQuery.parseJSON(data)
                renderHTML(obj);
            },
            error: function(error){
                alert("Books not found");
            },
        })
    });
    $.ajax({
        url: "data/" + txt,
        datatype: 'json',
        success: function(data){
            var obj = jQuery.parseJSON(data)
            renderHTML(obj);
        },
        error: function(error){
            alert("Books not found COBA");
        }
    });

var container = document.getElementById("myTable")
function renderHTML(data){
    info = "<tbody>";
    $("tbody").text("");
    console.log(data);
    for(i = 0; i < data.items.length; i++){
        info += "<tr>" 
        + "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" 
        + "<td><img class='img-fluid' style='width:20vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" 
        + "<td class='align-middle' style='text-align: center'>" + data.items[i].volumeInfo.title + "</td>"
        + "<td class='align-middle' style='text-align: center'>" + data.items[i].volumeInfo.authors + "</td>"
        + "<td class='align-middle' style='text-align: center'>" + data.items[i].volumeInfo.publisher + "</td>" 
        + "<td class='align-middle' style='text-align: center'>" + data.items[i].volumeInfo.publishedDate + "</td>" 
        + "<td class='align-middle' style='text-align: center'>" + "<img id='heart" + i + "' onclick='favourite(this.id)' width='25px' src='https://image.flaticon.com/icons/svg/149/149217.svg'>" + "</td></tr>";
    }
    container.insertAdjacentHTML('beforeend', info + "</tbody>");
    checkLike();
}
});

var countFavs = 0;
function checkLike(){
    $.ajax({
        type: 'GET',
        url: '/books/see-like/',
        datatype : 'json',
        success: function(data){
            for(var i=1; i <= data.message.length; i++){
                console.log(data.message[i-1]);
                var id = data.message[i-1];
                var td = document.getElementById(id);
                if(td!=null){
                    td.className = 'clicked';
                    td.src = 'https://image.flaticon.com/icons/svg/148/148836.svg';
                }
                $('#countFavs').html(data.message.length);
            }
            // $('tbody').html(print);
        }

    });
};

function favourite(id){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var user = document.getElementById(id);
    var liked = 'https://image.flaticon.com/icons/svg/148/148836.svg';
    var unliked = 'https://image.flaticon.com/icons/svg/149/149217.svg';
    if(user.className == 'checked'){
        $.ajax({
            url: "/books/unlike/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                countFavs = result.message;
                user.className = '';
                user.src = unliked;
                $('#countFavs').html(countFavs);
            },
            error: function(errorMessage){
                alert("Something is wrong");
            }
        });
    }else{
        $.ajax({
            url: "/books/like/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                console.log(user);
                countFavs = result.message;
                user.className = 'checked';
                user.src = liked;
                $('#countFavs').html(countFavs);
            },
            error: function(errorMessage){
                alert("Something is wrong like");
            }
        })
    }
}


