$(document).ready(function(){
    $(".accordion").on("click", ".title", function() {
        $(this).toggleClass("active").next().slideToggle();
      });
        $("#toggleButton").on({
        click: function(){
            $("body").css("background-color", "#1B1B1E");
            $("#headerText").css("color", "#ffffff");
            $(".longtext").css("color", "#ffffff");
            $("#header3").css("color", "#ffffff");
            $(".accordion").css("background-color", "#1B1B1E");
            $(".title").css("color", "#ffffff");
            $(".panel").css("color", "#ffffff");
            $("#toggleButton").text("Day Mode");
            $("#toggleButton").css("color","#1B1B1E");
            $("article").css("color","#1B1B1E");
      },
        dblclick: function(){
            $("body").css("background-color", "#ffffff");
            $("#headerText").css("color", "#1B1B1E");
            $(".longtext").css("color", "#1B1B1E");
            $("#header3").css("color", "#1B1B1E");
            $(".accordion").css("background-color", "#ffffff");
            $(".title").css("color", "#1B1B1E");
            $(".panel").css("color", "#1B1B1E");
            $("#toggleButton").text("Night Mode");
            $("#toggleButton").css("color","#ffffff");
            $("article").css("color","#1B1B1E");
      },
      })
});