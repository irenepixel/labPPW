var valid = false;
var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if(document.cookie.length > 0){
        c_start = document.cookie.indexOf(c_name + "=");
        if(c_start != -1){
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrftokenSafeMethod(method){
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function validEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isValid(){
    if(($("#id_email").val()==null || $("#id_email").val()=="") || ($("#id_password").val()==null || $("#id_password").val()=="") || 
    ($("#id_firstName").val()==null || $("#id_firstName").val()=="") || ($("#id_lastName").val()==null || $("#id_lastName").val()=="")){
        return false;
    }
    return true;
}

function unsub(id){
    var allId = document.getElementById(id);
    allId.remove();
    $.ajax({
        method: "POST",
        url: 'unsubscribe/',
        dataType: 'json',
        data: {
            'email' : id,
        },
    });
}

$(document).ready(function(){
    $.ajaxSetup({
        beforeSend: function(xhr, settings){
            if(!csrftokenSafeMethod(settings.type) && !this.crossDomain){
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $.ajax({
        url: 'data/',
        dataType: 'json',
        success: (function(obj){
            var i;
            for (i=0; i<obj.items.length; i++){
                var firstName = obj.items[i]['firstName'];
                var lastName = obj.items[i]['lastName'];
                var email= obj.items[i]['email'];
                $('#data').append('<tr id = '+email+'><td>'+firstName+'</td><td>'+lastName+'</td><td>'+email+'</td><td><button class="btn btn-info" onclick=unsub("'+email+'")>Unsubsribe</button></td></tr>');
            }
        })
    });

$("form").submit(function(event){
    $.ajax({
        method: "POST",
        url: 'submit/',
        data: {
            'firstName' : $("#id_firstName").val(),
            'lastName' : $("#id_lastName").val(),
            'email' : $("#id_email").val(),
            'password' : $("#id_password").val(),
        },
        dataType: 'json',
        success: function(msg){
            $( "#dialog" ).dialog();
                $( "#dialog" ).html("<p>"+msg['result']+"</p>");
                $("#id_firstName").val('');
                $("#id_lastName").val('');
                $("#id_email").val('');
                $("#id_password").val('');
        },
            error: function(msg){
                alert('Email not sent');
        },

    }),
    event.preventDefault();
});

$("#id_firstname").change(function(){
    if(isValid() && valid){
        $("#submit")[0].disabled=false;
    }
    else{
        $("#submit")[0].disabled=true;
    }
});

$("#id_lastname").change(function(){
    if(isValid() && valid){
        $("#submit")[0].disabled=false;
    }
    else{
        $("#submit")[0].disabled=true;
    }
});

$("#id_password").change(function(){
    if(isValid() && valid){
        $("#submit")[0].disabled=false;
    }
    else{
        $("#submit")[0].disabled=true;
    }
});

$("#id_email").change(function(){
    email = $(this).val();
    $.ajax({
        method: "POST",
        url: 'validation/',
        data: {
            'email': email
        },
        dataType: 'json',
        success: function(data){
            if(validEmail(email)==false){
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
                $('#waitUp').html('Enter a correct email address');
                valid = false;
            }
            else if (data.isTaken) {
                alert("Email is taken");
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
                $('#waitUp').html('Use other email address');
                valid = false;
            }
            else{
                if(isValid()){
                    $("#submit")[0].disabled=false;	
                }
                valid = true;
                $('#id_email').css("border-color", "white");
                $('#waitUp').html('');
            }
        }
    });
});
})

