import java.io.*;
import java.util.*;

public class SDA1819T3 {
//
//    public static AVLTree tree;
    public static String explorer;

    public static void main(String[] args) throws IOException {
        AVLTree tree = new AVLTree();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        int allCommands = Integer.parseInt(reader.readLine());
        String commands1[] = reader.readLine().split(" ");
        for(int i = 0; i < Integer.parseInt(commands1[0]); i++){
            String commands2[] = reader.readLine().split(" ");
            if (commands2[0].equals("GABUNG")) {
                explorer = commands2[1];
                tree.root = tree.insert(tree.root, explorer);
            }
            else if(commands2[0].equals("CEDERA")){
                explorer = commands2[1];
                tree.root = tree.deleteNode(tree.root, explorer);
            }
            else if(commands2[0].equals("PARTNER")){
                explorer = commands2[1];
                tree.partner(tree.root, explorer);
            }
            else if(commands2[0].equals("MENYERAH")){
                explorer = commands2[1];
                tree.gaveUp(tree.root, explorer);
            }
            else if(commands2[0].equals("PRINT")){
                tree.printPreOrder(tree.root);
                System.out.println();
                tree.printPostorder(tree.root);
            }
        }
    }
}

    class Node implements Comparable<Node> {
        public String key;
        public int height;
        public Node left;
        public Node right;
        public Node parent;

        public Node(String current) {
            this.key = current;
            height = 1;
            left = null;
            right = null;
            parent = null;
        }

        @Override
        public int compareTo(Node otherNode) {
            return this.key.compareTo(otherNode.key);
        }
    }

    class AVLTree{
        Node root;

        int height(Node current){
            if(current == null){
                return 0;
            }
            return current.height;
        }

        int max(int prev, int current){
            return (prev > current) ? prev : current;
        }

        public int balanceNode(Node current){
            if (current == null){
                return 0;
            }
            return height(current.left) - height(current.right);
        }

        public void partner(Node current, String key){
            System.out.println(current.key);
//          System.out.println(current.parent.key);
            while(key.compareTo(current.key) != 0){
                if(key.compareTo(current.key) < 0){
                    current = current.left;
                    if(current.key.equals(key)) {
                        current = current.left;
                        break;
                    }
                    else {
                        continue;
                    }

                }
                else {
                    current = current.right;
                    if(current.key.equals(key)) {
                        current = current.right;
                        break;
                    }
                    else {
                        continue;
                    }
                }
            }
            System.out.println(current.key);
            System.out.println(current.parent.key);
            if(current.parent == null){
                System.out.println("TIDAK ADA");
            }
            else if(current.parent.left == current && current.parent.right == null){
                System.out.println("TIDAK ADA");
            }
            else if(current.parent.right == current && current.parent.left == null){
                System.out.println("TIDAK ADA");
            }
            else {
                if(key.compareTo(current.key) < 0){
                    System.out.println(current.parent.right);
                }
                if(key.compareTo(current.key) > 0){
                    System.out.println(current.parent.left);
                }
            }
        }

        Node rightRotate(Node current){
            Node now = current.left;
            Node prev = now.right;

            if(current.parent == null){
                now.parent = null;
            }

            now.right = current;
            current.parent = now;
            current.left = prev;

            if(prev == null){
            }else{
                prev.parent = current;
            }

            current.height = max(height(current.left), height(current.right))+1;
            now.height = max(height(now.left), height(now.right)) + 1;

            return now;
        }

        Node leftRotate(Node current){
            Node now = current.right;
            Node prev = now.left;

            if(current.parent == null){
                now.parent = null;
            }

            now.left = current;
            current.parent = now;
            current.right = prev;

            if(prev == null){
            }else{
                prev.parent = current;
            }

            current.height = max(height(current.left), height(current.right))+1;
            now.height = max(height(now.left), height(now.right))+1;

            return now;
        }

        Node minValueNodeDelete(Node node)
        {
            Node current = node;
            while (current.left != null)
                current = current.left;
            return current;
        }

        Node deleteNode(Node root, String key) {
            if (root == null)
                return root;
            if (key.compareTo(root.key) < 0)
                root.left = deleteNode(root.left, key);
            else if (key.compareTo(root.key) > 0)
                root.right = deleteNode(root.right, key);
            else {
                if ((root.left == null) || (root.right == null)) {
                    Node temp = null;
                    if (temp == root.left)
                        temp = root.right;
                    else
                        temp = root.left;

                    // No child case
                    if (temp == null) {
                        temp = root;
                        root = null;
                    } else // One child case
                        root = temp; // Copy the contents of
                    // the non-empty child
                } else {
                    Node temp = minValueNodeDelete(root.right);

                    // Copy the inorder successor's data to this node
                    root.key = temp.key;

                    // Delete the inorder successor
                    root.right = deleteNode(root.right, temp.key);
                }
            }
            if (root == null)
                return root;
            root.height = max(height(root.left), height(root.right)) + 1;
            int balance = balanceNode(root);
            if (balance > 1 && balanceNode(root.left) >= 0)
                return rightRotate(root);

            // Left Right Case
            if (balance > 1 && balanceNode(root.left) < 0)
            {
                root.left = leftRotate(root.left);
                return rightRotate(root);
            }

            // Right Right Case
            if (balance < -1 && balanceNode(root.right) <= 0)
                return leftRotate(root);

            // Right Left Case
            if (balance < -1 && balanceNode(root.right) > 0)
            {
                root.right = rightRotate(root.right);
                return leftRotate(root);
            }

            return root;
        }

        Node minValueNodeGaveUp(Node node)
        {
            Node current = node;
            while (current.right != null)
                current = current.right;
            return current;
        }

        Node gaveUp(Node root, String key) {
            if (root == null)
                return root;
            if (key.compareTo(root.key) < 0)
                root.left = deleteNode(root.left, key);
            else if (key.compareTo(root.key) > 0)
                root.right = deleteNode(root.right, key);
            else {
                if ((root.left == null) || (root.right == null)) {
                    Node temp = null;
                    if (temp == root.left)
                        temp = root.right;
                    else
                        temp = root.left;

                    // No child case
                    if (temp == null) {
                        temp = root;
                        root = null;
                    } else // One child case
                        root = temp; // Copy the contents of
                    // the non-empty child
                } else {
                    Node temp = minValueNodeGaveUp(root.left);

                    // Copy the inorder successor's data to this node
                    root.key = temp.key;

                    // Delete the inorder successor
                    root.left = deleteNode(root.left, temp.key);
                }
            }
            if (root == null)
                return root;
            root.height = max(height(root.left), height(root.right)) + 1;
            int balance = balanceNode(root);
            if (balance > 1 && balanceNode(root.left) >= 0)
                return rightRotate(root);

            // Left Right Case
            if (balance > 1 && balanceNode(root.left) < 0)
            {
                root.left = leftRotate(root.left);
                return rightRotate(root);
            }

            // Right Right Case
            if (balance < -1 && balanceNode(root.right) <= 0)
                return leftRotate(root);

            // Right Left Case
            if (balance < -1 && balanceNode(root.right) > 0)
            {
                root.right = rightRotate(root.right);
                return leftRotate(root);
            }

            return root;
        }

        Node insert(Node current, String key){
            if(current == null)
                return (new Node(key));
            if(key.compareTo(current.key) < 0) {
                current.left = insert(current.left, key);
                current.left.parent = current;
            }
            else if(key.compareTo(current.key) > 0) {
                current.right = insert(current.right, key);
                current.right.parent = current;
            }
            else
                return current;

            current.height = 1 + max(height(current.left), height(current.right));
            int balance = balanceNode(current);

            if(balance > 1 && key.compareTo(current.left.key) < 0)
                return rightRotate(current);
            if(balance < -1 && key.compareTo(current.right.key) > 0)
                return leftRotate(current);
            if (balance > 1 && key.compareTo(current.left.key) > 0) {
                current.left = leftRotate(current.left);
                return rightRotate(current);
            }
            if (balance < -1 && key.compareTo(current.right.key) < 0) {
                current.right = rightRotate(current.right);
                return leftRotate(current);
            }
            return current;
        }
        void printPreOrder(Node current){
            if (current == null)
                return;
                System.out.print(current.key + ";");
                printPreOrder(current.left);
                printPreOrder(current.right);
            }
        void printPostorder(Node current) {
            if (current == null)
                return;
            printPostorder(current.left);
            printPostorder(current.right);
            System.out.print(current.key + ";");
        }

    }
