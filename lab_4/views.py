from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    response={}
    return render(request, 'homepix.html', response)

def about(request):
    response={}
    return render(request, 'aboutpix.html', response)

def biodata(request):
    response={}
    return render(request, 'biodatapix.html', response)

def education(request):
    response={}
    return render(request, 'educationpix.html', response)

def techskills(request):
    response={}
    return render(request, 'techskillspix.html', response)

def whativedone(request):
    response={}
    return render(request, 'whativedonepix.html', response)

def perspective(request):
    response={}
    return render(request, 'perspectivepix.html', response)

def guestbook(request):
    response={}
    return render(request, 'guestbookpix.html', response)




    
