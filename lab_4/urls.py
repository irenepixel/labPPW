from django.urls import path
from . import views
app_name = 'lab_4'
urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('biodata/', views.biodata, name='biodata'),
    path('education/', views.education, name='education'),
    path('techskills/', views.techskills, name='techskills'),
    path('whativedone/', views.whativedone, name='whativedone'),
    path('perspective/', views.perspective, name='perspective'),
    path('guestbook/', views.guestbook, name='guestbook'),
]

