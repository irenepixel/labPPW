from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Landing
from .forms import Landing_Form

response = {}

def landing(request):
    form = Landing_Form(request.POST or None)
    all_status = Landing.objects.all()
    response = {
        "all_status" : all_status
    }
    response['form'] = form

    if (request.method == 'POST' and form.is_valid()):
        status = request.POST.get("status")
        Landing.objects.create(status=status)
        return redirect('landing')   
    else:
        return render(request, 'landing.html', response)

def aboutme(request):
    response={}
    return render(request, 'aboutme.html', response)







