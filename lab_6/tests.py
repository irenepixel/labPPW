# from django.test import TestCase, Client
# from django.http import HttpRequest
# from django.urls import resolve
# from django.utils import timezone
# from .views import landing, aboutme
# from .models import Landing
# from .forms import Landing_Form
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from selenium import webdriver
# import unittest
# import time

# class Lab6UnitTest(TestCase):

#     def test_lab_6_url_is_exist(self):
#         response = Client().get('/lab_6/')
#         self.assertEqual(response.status_code, 200)

#     def test_lab6_using_landing_func(self):
#         found = resolve('/lab_6/')
#         self.assertEqual(found.func, landing)

#     def test_html_text(self):
#         request = HttpRequest()
#         response = landing(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('Halo, apa kabar?', html_response)

#     def test_model_can_create_new_status_and_has_str_function(self):
#         #Creating a new status
#         new_status = Landing.objects.create(status='This is not an easy life')

#         #Retrieving all available status
#         counting_all_available_status = Landing.objects.all().count()
#         self.assertEqual(counting_all_available_status,1)

#         #Testing str function
#         self.assertEqual(str(new_status),new_status.status)

#     def test_aboutme_url_is_exist(self):
#         response = Client().get('/lab_6/aboutme/')
#         self.assertEqual(response.status_code, 200)

#     def test_aboutme_using_aboutme_func(self):
#         found = resolve('/lab_6/aboutme/')
#         self.assertEqual(found.func, aboutme)

#     def test_aboutme_header_text(self):
#         request = HttpRequest()
#         response = aboutme(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Halo, perkenalkan</p>', html_response)

#     def test_aboutme_photos(self):
#         request = HttpRequest()
#         response = aboutme(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('aboutme.jpg', html_response)

#     def test_aboutme_description_text(self):
#         request = HttpRequest()
#         response = aboutme(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Irene, mahasiswi Faskultas Ilmu Komputer 2017.</p>', html_response)

# class Lab6FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(Lab6FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab6FunctionalTest, self).tearDown()

#     def test_can_start_a_list_and_retrieve_it_later(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/')
#         time.sleep(3)
#         search_box = selenium.find_element_by_class_name('form-control')
#         search_box.send_keys('Coba Coba')
#         search_box.submit()
#         assert 'Coba Coba' in selenium.page_source
#         time.sleep(10)
    
#     def test_title_is_correct(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/')
#         self.assertIn("Landing Page", selenium.title)

#     def test_title_is_correct(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/aboutme/')
#         self.assertIn("About Me", selenium.title)

#     # def test_header_is_correct(self):
#     #     selenium = self.selenium
#     #     selenium.get('http://localhost:8000/lab_6/')
#     #     hello = selenium.find_element_by_name('heading').text
#     #     self.assertIn("Halo, apa kabar?", hello)

#     def test_style_button(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/aboutme/')
#         button = selenium.find_element_by_tag_name('button')
#         self.assertIn('submit', button.get_attribute('type'))

  

    


        



        
        


