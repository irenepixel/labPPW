from django import forms
from .models import Landing

class Landing_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    status = forms.CharField(label = "tentang hari ini", required = True, widget = forms.Textarea(attrs=attrs) , max_length = 300)
