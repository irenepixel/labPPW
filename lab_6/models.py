from django.db import models

# Create your models here.

class Landing(models.Model):
    status = models.TextField(max_length = 300)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.status
