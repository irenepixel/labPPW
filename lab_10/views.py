from django.shortcuts import render, redirect
from django.db import IntegrityError
from django.http import HttpResponse
from .models import subscribe
from .forms import subscribeForm
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.db import IntegrityError
import json

response = {}
def displayForm(request):
    form = subscribeForm(request.POST or None)
    response['form'] = form
    return render(request, 'subscribe.html', response)

def runForm(request):
    try:
        if(request.method == "POST"):
             firstName = request.POST.get('firstName', None)
             lastName = request.POST.get('lastName', None)
             email = request.POST.get('email', None)
             password = request.POST.get('password', None)
             subscribe.objects.create(firstName=firstName, lastName=lastName, email=email, password=password)
             return HttpResponse(json.dumps({'result' : 'Thankyou ' + firstName + ' for subscribing!'}))
        else:
            return HttpResponse(json.dumps({'result' : 'fail'}))

    except IntegrityError as e:
        return HttpResponse(json.dumps({'result' : 'fail'}))

@csrf_exempt
def emailValidation(request):
    theEmail = request.POST.get('email', None)
    validation = {
        'isTaken' : subscribe.objects.filter(email=theEmail).exists()
    }
    return JsonResponse(validation)

def subscribersList(request):
        response['fill'] = subscribe.objects.all()
        return render(request, 'subscriberList.html', response)

def reqJson(request):
        subscribers = subscribe.objects.all()
        listAll = list()
        for i in subscribers:
                user = {'firstName' : i.firstName, 'lastName' : i.lastName, 'email' : i.email}
                listAll.append(user)
        return HttpResponse(json.dumps({'items': listAll}))

def unsub(request):
        email = request.POST.get('email', None)
        subscribe.objects.all().filter(email=email).delete()
        return HttpResponse(json.dumps({'items':email}))
        
