from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import displayForm, runForm, emailValidation, subscribersList, reqJson, unsub
from .forms import subscribeForm
from .models import subscribe
from django.db import IntegrityError

class FormTest(TestCase):
    def test_lab10_url_is_exist(self):
        response = Client().get('/lab_10/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_lab10_url_is_exist2(self):
        response = Client().get('/lab_10/subscribers/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'subscriberList.html')
    
    def test_lab10_url_is_exist2(self):
        response = Client().get('/lab_10/validation/')
        self.assertEqual(response.status_code,200)
        response = emailValidation(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('false', html)

    def test_lab10_url_is_exist3(self):
        response = Client().get('/lab_10/subscribers/data/')
        self.assertEqual(response.status_code,200)
        subscribe.objects.create( firstName="irene", lastName="test", email="irene15@gmail.com", password="ren1234567")
        response = reqJson(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('items', html)
        self.assertIn('irene', html) 

    def test_lab10_url_is_exist4(self):
        response = Client().get('/lab_10/unsubscribe/')
        self.assertEqual(response.status_code,200)
        response = unsub(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('items', html)

    def test_post(self):
        firstname = 'test'
        lastname = 'test2'
        email= 'test@testcase.com'
        password= 'test1234567'
        response_post = Client().post('/lab_10/submit/',{ 'firstName': firstname, 'lastName' : lastname,'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)

    
    def test_post2(self):
        firstname = 'test'
        lastname = 'test2'
        email= ''
        password='test1234567'
        response_post = Client().get('/lab_10/submit/',{ 'firstName': firstname, 'lastName' : lastname,'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)
        

    def test_postf(self):
        response_post = Client().post('/lab_10/submit/',{'firstName': 'irene', 'lastName' : 'pixelyne','email':'rene@gmail.com', 'password':'rene12345'})
        response_post1 = Client().post('/lab_10/submit/',{'firstName': 'pixel', 'lastName' : 'rene','email':'pixel@rene.com', 'password':'12345pixel'})
        self.assertEqual(response_post1.status_code, 200)
        
        
