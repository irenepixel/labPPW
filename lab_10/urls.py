from django.contrib import admin
from django.urls import path
from . import views
app_name= 'lab_10'
urlpatterns = [
    path('', views.displayForm, name='displayForm'),
	path('submit/', views.runForm, name='runForm'),
	path('validation/', views.emailValidation, name='emailValidation'),
	path('subscribers/', views.subscribersList, name='subscribers'),
	path('subscribers/data/', views.reqJson, name='data'),
	path('unsubscribe/', views.unsub, name='unsubscribes'),
]