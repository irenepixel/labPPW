from django import forms

class subscribeForm(forms.Form):

    invalid_message={
        'required' : 'Please fill out this field',
        'invalid' : 'Please fill out with valid Input',
    }

    firstname_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your first name',
    }

    lastname_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your first name',
    }

    email_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter a valid email address',
    }

    password_attrs={
        'type' : 'password',
        'class' : 'todo-form-input',
        'placeholder' : 'Password should be a combination of Alphabets and Numbers',
    }

    firstName = forms.CharField(label="First Name", required=True, max_length=30, widget=forms.TextInput(attrs = firstname_attrs))
    lastName = forms.CharField(label="Last Name", required=True, max_length=30, widget=forms.TextInput(attrs = lastname_attrs))
    email = forms.EmailField(label="Email", required=True, max_length=30, widget=forms.TextInput(attrs = email_attrs))
    password = forms.CharField(label="Password", required=True, max_length=30, widget=forms.PasswordInput(attrs = password_attrs))