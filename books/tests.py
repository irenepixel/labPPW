from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index, data
import unittest
import time

# Create your tests here.
class BooksUnitTest(TestCase):
    def test_books_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/books/data/quilt')
        self.assertEqual(response.status_code, 200)

    def test_lab9_using_booklist_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'booklist.html')

    def test_books_using_index_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, index)

class BooksFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(BooksFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(BooksFunctionalTest, self).tearDown()

    def test_layout_page_title(self):
        selenium = self.selenium
        selenium.get('http://lab-irene-ppw-f.herokuapp.com/books/')
        self.assertIn('Favorite Books', selenium.title)
        time.sleep(2)

    # def test_layout_header_with_css_property(self):
    #     selenium = self.selenium
    #     selenium.get('http://lab-irene-ppw-f.herokuapp.com/books/')
    #     content = selenium.find_element_by_tag_name('p').value_of_css_property('color')
    #     self.assertIn('rgba(33, 37, 41, 1)', content)
    #     time.sleep(2)
   



