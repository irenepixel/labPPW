from django.contrib import admin
from django.urls import path
from . import views

app_name= 'books'
urlpatterns = [
    path('', views.index, name='index'),
    path('data/<str:txt>', views.data, name='data'),
    path('like/', views.like, name="like"),
    path('unlike/', views.unlike, name="unlike"),
    path('see-like/', views.getLike, name="see-like"),
]