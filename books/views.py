from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
import requests
import json

response={}
def data(request, txt):
	json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + txt).json()
	json_parse = json.dumps(json_read)
	return HttpResponse(json_parse)

def index(request):
    if request.user.is_authenticated and "like" not in request.session:
        request.session["email"]=request.user.email
        request.session["sessionid"]=request.session.session_key
        request.session["like"]= []
    return render(request, 'booklist.html')
    
@csrf_exempt
def like(request):
    if(request.method=="POST"):
        lks = request.session["like"]
        if request.POST["id"] not in lks :
            lks.append(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lks
        response["message"]=len(lks)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")

@csrf_exempt
def unlike(request):
    if(request.method=='POST'):
        lks = request.session["like"]
        if request.POST["id"] in lks :
            lks.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lks
        response["message"]=len(lks)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")

def getLike(request):
    if request.user.is_authenticated :
        if(request.method == "GET"):
            if request.session['like'] is not None:
                response["message"]=request.session["like"]
            else:
                response["message"]="NOT ALLOWED"
        else:
            response["message"] = ""
        return JsonResponse(response)









