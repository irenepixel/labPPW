from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from books import views as viewsBooks
from .views import *
import unittest


# Create your tests here.
class finalUnitTest(TestCase):
    def test_final_url_exist(self):
        response = Client().get('/final/')
        self.assertEqual(response.status_code, 200)
    
    def test_final_using_index_func(self):
        found = resolve('/final/')
        self.assertEqual(found.func, final)

    def test_final_html_template(self):
        response = Client().get('/final/')
        self.assertTemplateUsed(response, 'final.html')
    
    def test_final_url_logout_exist(self):
        response = Client().get('/final/logout/')
        self.assertEqual(response.status_code, 200)
