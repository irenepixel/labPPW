from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Schedule(models.Model):
    day = models.CharField(max_length=20)
    date = models.DateField()
    time = models.TimeField()
    name = models.CharField(max_length=100)
    place = models.CharField(max_length=140)
    category = models.CharField(max_length=100)
    
    


 
