from django.contrib import admin
from django.urls import path
from . import views
app_name = 'lab_5'
urlpatterns = [
	path('', views.displayschedule, name='displayschedule'),
	# path('displayschedule/', views.displayschedule, name='displayschedule'),
	path('makeschedule/', views.makeschedule, name='makeschedule'),
	path('delete/', views.delete, name='delete'),
]