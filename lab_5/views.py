from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Schedule

# Create your views here.

def displayschedule(request):
    response = {}
    schedules = Schedule.objects.all()
    response = {
        "schedules" : schedules
    }
    return render(request, 'displayschedule.html', response)


def makeschedule(request):
    form = ScheduleForm(request.POST or None)
    response = {}
    if(request.method == "POST"):
        if(form.is_valid()):
            day = request.POST.get("day")
            date = request.POST.get("date")
            time = request.POST.get("time")
            name = request.POST.get("name")
            place = request.POST.get("place")
            category = request.POST.get("category")
            Schedule.objects.create(day=day, date=date, time=time, name=name, place=place, category=category)
            return redirect('displayschedule')
        else:
            return render(request, 'makeschedule.html', response)
    else:
        response['form'] = form
        return render(request, 'makeschedule.html', response)


def delete(request):
    response={}
    schedules = Schedule.objects.all().delete()
    response['schedule'] = schedules
    return render(request, 'displayschedule.html', response)
