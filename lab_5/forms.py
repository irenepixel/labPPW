from django import forms
from .models import Schedule

class ScheduleForm(forms.Form):
    OPTION =(
        ('1', 'Education'),
        ('2', 'Entertaiment'),
        ('3', 'Sports'),
        ('4', 'Arts'),
        ('5', 'Socials'),
        ('6', 'Others'),
    )
    day = forms.CharField(label = "Day",  required = True, max_length=100)
    date = forms.DateField(label = "Date", required = True, widget=forms.DateInput(attrs={'type':'date'}))
    time = forms.TimeField(label = "Time", required = True, widget=forms.TimeInput(attrs={'type':'time'}))
    name = forms.CharField(label = "Activity", required = True, max_length=100)
    place = forms.CharField(label = "Place", required = True, max_length=140)
    category = forms.ChoiceField(label = "Category", required = True, widget=forms.Select(), choices=OPTION)
